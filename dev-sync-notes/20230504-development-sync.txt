Development sync for May 04, 2023, 1300 UTC


Agenda

    * What have you been working on this week? 
        * Save suggestions until after the meeting is over, please
    * Are you having trouble with anything?

    * Discuss points in backlog... should we put more stuff on the board?

    * When's our next sync?


Minutes

Ratchanan

	* Race condition fix for SIM pin lock code entry pad (not coming up at boot)
	* Fix enabling developer mode while the phone is not plugged into a computer (lomiri-system-settings)
	* Fix error notification (from within network indicator) if hotspot activation fails
		* Needs some follow-up investigation due to the fact of code/translation strings duplications in lomiri-indicator-network and lomiri-system-settings (maybe even causing duplicate notifications)
	* Scaling settings not read correctly by the spinner correctly, cause: spinner haven't been migrated to deviceinfo, yet. Work in process, currently.
	* Still blocked by Mike regarding mobile-broadband-provider info...

Jami

	* Looking into UBports recovery for Halium 12. Currently blank screen only, with no USB. Still looking for a way to debug this.

Alfred

	* Fixing Xperia X port, including fixing the long standing issue that the camera would show half of its view finder in green color (on some occasions, race condition).
	* qtcamera race time condition analysis

Nikita

	* Video recording on Halium 12: 
	* Enable CONFIG_SYSVIPC without breaking GKI ABI: https://gitlab.com/ubports/porting/community-ports/android11/volla-x23/kernel-volla-mt6789/-/commit/c360d6f7b22ab710a27193f62669f5a257cd259d
	* Why X11 apps in landscape on phone screen do not have touch input working for right half of the screen (screen orientation issue):
		* Wayland applications do not get updated about display size changes, only the window gets resized
		* Need to cherry-pick https://github.com/MirServer/mir/commit/1c00e7a625c98b5a1a3ceeed1ad4053d5e8e8a3a for Mir server
		* However, handle_configuration_change never gets triggered as Lomiri rotates the Shell in QML and does not propagate display attribute changes to mir server
		* Can be seen in mirout tool - display orientaton always stays as normal
