Development sync, Tuesday 18 October 2022

Guido:
    - Core apps renaming
    - Look into GitHub -> GitLab renaming
    - import new version of upstream accounts-qml-module
    - location-service: troubleshoot crash on start
    - Test lxc-android-config "the rest" migration
	    - Pixel 3a XL
    - usb-moded: support USB_PD
    - MTP on Pixel 3a XL
    - trust-store

Jami:
    - Volla Phone 22: Looking into kernel crash to orange screen if Wi-Fi toggled too fast
    - lomiri-system-settings: Figured out python-dbusmock commit causing test failures on Debian 12 and newer
	    - Will adapt tests to pass there but still also work on focal 20.04
    
Nikita:
    - Volla Phone 22: Power saving with Wi-Fi enabled
    - Volla Phone 22: Debugging volume adjustment in voice calls
    - Figuring out how to boot Qualcomm 5.4 kernel (it's even more a mess now)

Ratchanan:
    - Networkmanager:
        - debugged failure to reconnect data connection after leaving flight-mode
	    - context property
	    - waiting for maintainer feedback
    - added dumpling, cheeseburger added to system-image
    - added bluebinder to patterns

Alfred
        - pocketvm app
        - GTK3 patched fro hybris/mir support
        - planning IRQ-balancing daemon for halium container for visual perfomance
        - experimenting with box64 for amd64 support on arm
	        - possibly as click app
	
Jonatan:
    - new clickable release for focal
    - feedback on account-hook exception for review tool
    - openstore now supports focal, same app can be offered separately for focal and xenial
	    - clickable publish works
