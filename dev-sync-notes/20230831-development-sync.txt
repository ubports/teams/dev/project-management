UBports DevSync Notes 2023-08-31

What you have been working on? What can be unblocked for you?

FOSDEM 2024 anyone???

Ratchanan:
	* Finalizing usage of lineageos-apndb, in use from now on.
	* mobile-broadband-provider-info is not used anymore
	* Help Abdullah AL Shohag with Bengali language support in lomiri-keyboard
	* a-i-power: bump to 23.6.0 , problem with newly added RDA support in a-i-power
	* content-hub: make it more secure (pretty complex!)
	* Alfred will unblock Ratchanan by reviewing: https://github.com/Halium/hybris-patches/pull/69 (and co.)

Jami:
	* Working on Volla 22, Halium 12 (inclusion into OTA-3? Problematic, hard-to-say for now); Nikita: at the moment going back from Volla 22 with Halium 12 is not easily possible, so better wait until Halium 12 based Volla 22 is working really well.

Nikita:
	* Work on Volla X23 - trying to debug fingerprint
	* pulseaudio-module-droid notify audio HAL about headphone/headset connection state MR - working to reimplement in a more generic way
	* Misc Halium 12/13 and GKI stuff

Guido:
	* nothing to report

Lionel:
	bring (tried to) Lomiri UI Toolkit Gallery to OpenStore
	shipped QtCreator 11.0.2 to clickable docker ide image
	
Alfred:
	* finalizing haliumqsgcontext packaging, making sure it gets loaded across all application contexts
	* aethercast + JingPad
	* will re-visit the go_intent MR for network-manager, make it configurable for porters via wpa_supplicant config parameter in overlay
	* haliumqsgcontext -> leave it to porters to enable it (override profile.d/ file)
	* Alfred enabled haliumqsgcontext for JingPad (already merged), P3a (already merged), FP4 (already merged)
	* Probe schedtune in waydroid
	* Hardware-accelerated video decoding also merged last week.

Mike:
	* Ayatana Indicators work, release qmenumodel, merge in GTK-upgrade upstream
	* Discuss with Marius and Raoul his new role of community manager + start working on (yet another) roadmap for UT

