Development Sync December 29th, 2022 13:00 UTC

Guido
- improve locale building, select based on translation coverage on Weblate
- add workaround to make locale selection work with AccountsService
- fix AppArmor policy for connectivity check API
- work on replacement for focus check in lomiri-push-service

Alfred:
- fscrypt for home directories
- work on snap usage
- hardware video decoding with Morph
- fix camera access in Morph


Lionel
- work on removing the 4 digits pin code limitation ( system settings / Lormiri wizard and pin prompt )
- Worked on the "circle" pin code prompt
NEED HELP: Jenkins fail s at building Lomiri MRs ( fails on amd64)

Ratchanan
- locale generation & selection, fix US/CA exception
- Overlay improvement for FP4
