UBports DevSync Notes 2023-08-10

What you have been working on? What can be unblocked for you?

Ratchanan:
	* Implement Other-Vibrate settings for hfd-service (via AccountsService as setting store)
		* opt a: slightly complex to do things asynchronously in AccountsService
		* opt b: implement it per user setting instead (awkward approach)
		* choice: AccountsService, with a non-blocking implementation approach
		* settings will get cached

Guido:
	* not really any UBports related stuff

Mike:
	* not really any UBports related stuff

Jami:
	* not much work done, due to sickness and only just got back from "vacation"
	* re-enable video decoding in Morph Browser for X23

Lionel:
	* QtCreator update from 22.04 as part of clickable
		* Possibly do a session where Lionel shows his development workflows
	* Debug on really hide notifications when phone is locked (not working as proposed via UI)
	* Discuss with UI/UX team whether to blur the notification bubbles instead of hiding them entirely

Marius:
	* Strategy for UT development, what we landed on:
		* instead of focusing on dist-upgrade, spend time on community management, flourish the community again, bringing back Q&As, focus more on code reviews and technical debts
		* maybe lack of community activity related to Github -> GitLab migration
		* investigate how to improve community interaction / contributions
		* technical side:
			* focus on user papercuts, on annoying issues in the UI/UX
			* fixup packaging, CI/CD, etc. (e.g. do CI builds against Debian sid)
		* get ready for tool chain freeze in Ubuntu 24.04 and do the dist-upgrade bump immediately then, in parallel with UT 24.04 finalization development
		* release on the same date as Canonical Ltd. does, esp. to get media attention, share some media coverage with Ubuntu upstream, go in tandem with LTS releases
	* Technical roadmap ETA: 11th Aug 2023
	* Community management: Marius will pick up the task for now, but the long-term goal is to find someone who can take over the community management.
	* Date arranged for next Q&A: 19th Aug 2023
	* Usual stuff: research on VoLTE, desktop support, fix RC bugs of Lomiri packages in Debian

Alfred:
	* Finalizing the H.264 (et al.) hardware accelerated video decoding (was not fully functional, was not performing as well as it could); final patch coming today...
	* Work on Halium project "haliumqsgcontext", texture uploading to the screen via gralloc, multithreaded with up to 16 threads, switched QAnimationDriver, all in the same plugin: https://github.com/Halium/haliumqsgcontext
	* Needs proper packaging.



