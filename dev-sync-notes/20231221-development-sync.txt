
UBports DevSync Notes 2023-12-21

What you have been working on? What can be unblocked for you?

General:
	* OTA-4 freeze plan:
		- Devsync Jan 4: decide on last MRs.
		- freeze on 11th Jan 2024
		- release on 18th Jan 2024
		- backup date for release: 25th 2024
		- OTA-4 shall be out for FOSDEM 24 

Nikita:
	* Found the cause of missing audio in voice calls with 64-bit audio HAL (MAX_UINT used as a very large value => false with 64-bits) - workaround merged in hybris-patches
	* Started with ofono-binder-plugin-ext-mtk to eventually enable IMS/VoLTE on Volla X23/22
	* Helping people with ports

Jami:
	* Mostly looking into Volla 22 Halium 12 update. It boots now on staging system-image-channel

Raoul:
	* Sponsor proposal for FOSDEM, get-together for developers planned.
	* 2 talks have been accepted, so far (MaG -> VoLTE, OK -> PinePhone), one talk proposal still pending
	* Testing Lomiri on Ubuntu 23.10 on ThinkPad Yoga. Apps still fail to start.

Lionel:
	*  Small UI fix on address-book-app, waiting for final review of "privacy mode notification" MR in lomiri   

Ratchanan 
	* Good news: UBports Foundation agreed to sponsor me to FOSDEM, so see you!
	* Bad news: most of this week was spent preparing visa application documents.
	* That said, a few things get done.
		* QWE weird issue with timezone detection on some timezone. Turns out to be ICU + /etc/localtime redirection to /etc/writable/. 
			* https://gitlab.com/ubports/development/core/packaging/qtwebengine/-/issues/15
		* Get notifications in a-i-datetime work without Lomiri deps.
			* Mike: https://github.com/AyatanaIndicators/ayatana-indicator-datetime/pull/115#discussion_r1434018301
		* Try to add adbd-approver to Weblate.
			* Mike: please help. Turns out I don't have permision to add new project.
		* Review issues and MRs.

Mike:
	* Take part / follow https://bugs.debian.org/1057755 (proposal for QtWebengine -> rolling release in Debian)
	* Various code reviews & code merges

Guido:
	* no recent work on Ubuntu Touch, but joining because of upcoming Debian Lomiri Tablets project


