UBports DevSync Notes 2023-06-08

What you have been working on? What can be unblocked for you?

Topics:
	* OTA-2
	* Qt Auto-Scaling, how to proceed?
		* Test whether auto-scaling is enabled via Qt
			* https://doc.qt.io/qt-5/qcoreapplication.html#testAttribute
			* Qt::AA_EnableHighDpiScaling
			* https://doc.qt.io/qt-5/highdpi.html#high-dpi-support-in-qt
			* Update: not sure if the attribute is actually the source of truth for Qt itself. Need to read the Qt code further...
			* QHighDpiScaling::isActive()
		* Provide support for with/without Qt auto-scaling in the various places where we observe UI regressions.
		* @sunweaver will work on with/without patches for luitk and qqc2-suru-style


Mike:
	* Processing various/many merge requests.
	* Triage old merge ubports/xenial merge requests and suggest forwarding-porting / closing

Ratchanan:
	* Diagnose problem of the custom alarm sound in clock app not playing as selected by the user.
		* apparmor issue in media-hub
		* API migration to libeCal-2.0 was incomplete
	* Finish user-session-migration.
	* Volla 22 behaviour with newer WPA versions, research difference between key management algos in Android and wpa_supplicant in Ubuntu Touch), the handshake in AP mode should be hardware dependent as it happens completely in user space. Research is  on-going... (problems flashing the Volla 22 back and forth)
	* qtwebengine bump to 5.15.14

Jami:
	* Volla Phone X23: Still looking into USB cable mode detection on UT (usb-moded)

Lionel
	* Work on spread view, move new application to the foreground (Lomiri )

Marius
	* VoLTE quest continued...
	* desktop work, merging MRs
	* this work, will work on MRs submissions and reviews even more
	* (sorting bits and pieces that are locally and hidden in branches the coming week)

Sergey
	* Will work on qmlscene to C++ conversion for various apps.
	* Mike will file issues for components where this is required.

Nikita
	* not really worked on UT this week, more on the Volla phone
